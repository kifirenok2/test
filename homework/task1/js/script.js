"use strict";

window.addEventListener('load', task);

function task(){

      const ITEA_COURSES = ["Курс HTML & CSS",
                            "JavaScript базовый курс", 
                            "JavaScript продвинутый курс", 
                            "JavaScript Professional", 
                            "Angular 2.4 (базовый)", 
                            "Angular 2.4 (продвинутый)", 
                            "React.js", "React Native", 
                            "Node.js", 
                            "Vue.js"
                            ];
  
      task1V1(ITEA_COURSES);
      task1V2(ITEA_COURSES);
      task2(ITEA_COURSES);
      task3(ITEA_COURSES);

      function task1V1(arr){
    
        var result = [];
        
        arr.forEach(function(item){
          result.push(item.length);

        });
        
        console.log('Задание 1. версия 1(forEach), новый массив');
        console.log(result);
        //return result;
      };

      function task1V2(arr){

        var result = arr.map(function(item){
          return item.length;
        });

        console.log('Задание 1. версия 2(map), новый массив');
        console.log(result);
      };

      function task2(arr){
        console.log('Задание 2. сортировка');
        console.log(arr.sort());

        var div = document.getElementById('task2');
        
        createUl(arr, div);
      };

      function task3(arr){
        var btn = document.getElementById('btn-search');
        btn.addEventListener('click', search);
      
        function search(){
          var div = document.getElementById('search-result');
          div.innerHTML = '';
          var input = document.getElementById('search');
          var searchText = input.value.toLowerCase();

          var searchResult = arr.filter(function(item){
            return item.toLowerCase().indexOf(searchText) >= 0;
          });

          createUl(searchResult, div);
        };
      };

      function createUl(arr, div){
        var ul = document.createElement('ul');

        for( var i = 0; i < arr.length; i++ ){
          var li = document.createElement('li');
          li.innerHTML = arr[i];
          ul.appendChild(li);
        }

        div.appendChild(ul);

      };
       
};